### Примечания

Небольшая [npm библиотека](https://www.npmjs.com/package/show-dependencies-stargazers-count) для отображения в терминале количества звезд (в гитхабе), для зависимостей из файла package.json. Есть возможность настройки отображения в виде таблицы и в виде bar диаграммы, указывать направление сортировки.

Можно скачать пакет, используя

```
npm install -g show-dependencies-stargazers-count
```

и запустив в проекте с использованием nodejs, в корне которого есть package.json файл команду:

```
show-dependencies-stargazers-count
```

или, в более коротком формате

```
depstaco
```

Для UI в консоли использовались [blessed](https://github.com/chjj/blessed) и [blessed-contrib](https://github.com/yaronn/blessed-contrib). В этом небольшом проекте, если не ошибаюсь, впервые, после небольшого обучения, было применено на практике использование такой парадигмы, как функциональное программирование, при помощи библиотеки, позволяющей писать в JS в функциональном стиле - [ramda](https://github.com/ramda/ramda).

* [ava](https://github.com/avajs/ava) - тестирование
* [xo](https://github.com/sindresorhus/xo) - стайл линтер
